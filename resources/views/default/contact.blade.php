@extends('default.layout.layout')

@section('content')
    {{--{{dump(Session::all())}}--}}
    <div class="col-md-9">
        @if (count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li> {{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{--{{ dump(Session::all()) }}--}}
        <form method="post" action="{{route('contact')}}">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" name="email" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Site</label>
                <input type="text" class="form-control" name="site" value="{{old('site')}}" placeholder="site">
            </div>

            <div class="form-check">
                <label class="form-check-label">
                    <input type="checkbox" name="check" @if(old('check') == 'on') checked @endif   class="form-check-input">
                    Check me out
                </label>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">﻿
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection