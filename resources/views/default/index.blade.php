@extends('default.layout.layout')

@section('navbar')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('sidebar')
    @parent
    {{$script}}
    {{--{!! $script !!}--}}
    @{{var}}
    {{isset($vbar) ? $vbar : $title}}
    {{$vbar or $title}}

    @if (count($data) < 3)
        less 3
    @elseif(count($data) > 10)
        more 10
    @else
       else_w
    @endif


    <ul>
        @for($i = 0; $i< count($dataI); ++$i)
            <li>
               {{$dataI[$i]}}
            </li>
        @endfor
    </ul>

    <ul>
        @foreach($dataI as $item)
            <li>{{$item}}</li>
        @endforeach
    </ul>

    <ul>

        @forelse($data as $item)
            <li>{{$item}}</li>
            @empty
            No items
        @endforelse
    </ul>
    <ul>
        @each('default.list', $dataI, 'val');
    </ul>

    <h4>About</h4>
    <div class="cont">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias enim incidunt molestias nesciunt, nihil possimus provident quaerat soluta! Ad delectus dolore eius expedita facilis laborum optio rem rerum tempore ullam!
    </div>
    @endsection


@section('content')
    @include('default.content')
@endsection