<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
// getIndex по умолчанию используется для обращения к запросу, создаваемого маршрутизатора
    public function getIndex() {
        echo __METHOD__;
    }
    public function getCreate() {
        echo __METHOD__;
    }

    public function postIndex() {
        print_r($_POST);
    }
}

