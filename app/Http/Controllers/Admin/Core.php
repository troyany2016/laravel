<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Country;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class Core extends Controller
{
    protected static $articles;
    /**
     * return List materials
     */
    public function __construct()
    {
//        $this->middleware('mymiddle');
    }

    public static function addArticles($array){
       return self::$articles[] = $array;
    }

    public function getArticles(){

//            ------------SELECT----------------
//        $result = DB::table('articles')->get(); //"select * from `articles`"

//        $result = DB::table('articles')->first(); //"select * from `articles` limit 1"
//
//        $result = DB::table('articles')->value('name'); //"select `name` from `articles` limit 1""

//        DB::table('articles')->orderBy('id')->chunk(2, function($articles){
//
//                foreach ($articles as $article){
//                    Core::addArticles($article);
//                }
//            });
//        dump(self::$articles);
//
//        $result = DB::table('articles')->pluck('name'); //"select `name` from `articles`"
//
//        $result = DB::table('articles')->count(); //"select count(*) as aggregate from `articles`"
//
//        $result = DB::table('articles')->max('id'); //"select max(`id`) as aggregate from `articles`"
//
//        $result = DB::table('articles')->select('name', 'id', 'text')->get(); //"select `name`, `id`, `text` from `articles`"
//
//        $result = DB::table('articles')->distinct()->select('name', 'id', 'text')->get(); //"select distinct `name`, `id`, `text` from `articles`"

//        $query = DB::table('articles')->select('name'); //--------
//        $articles = $query->addSelect('text')->get(); //"select `name`, `text` from `articles`"
//
//            $articles = DB::table('articles')->select('name')->where('id', '=', '1')->get(); //"select `name` from `articles` where `id` = ?"

//        $articles = DB::table('articles')->select('name')
//                                            ->where('id', '=', '1')
//                                            ->where('name', 'like', '1%')
//                                            ->get(); //"select `name` from `articles` where `id` = ? and `name` like ?"

//            $articles = DB::table('articles')->select('name')
//                                            ->where('id', '=', '1')
//                                            ->where('name', 'like', '1%', 'or')
//                                            ->get(); //"select `name` from `articles` where `id` = ? or `name` like ?"
//
//        $articles = DB::table('articles')->select('name')
//            ->where([
//                ['id', '=', '1'],
//                ['name', 'like', '1%', 'or']
//            ])
//            ->get(); //"select `name` from `articles` where (`id` = ? or `name` like ?)"

//        $articles = DB::table('articles')->select('name')
//                                            ->where('id', '=', '1')
//                                            ->where('name', 'like', '1%', 'or')
//                                            ->orWhere('id', '<', 2)
//                                            ->get(); //"select `name` from `articles` where `id` = ? or `name` like ? or `id` < ?"

//        $articles = DB::table('articles')->whereBetween('id', [1, 5])->get(); //"select * from `articles` where `id` between ? and ?"
//
//        $articles = DB::table('articles')->whereNotBetween('id', [1, 5])->get(); //"select * from `articles` where `id` not between ? and ?"
//
//        $articles = DB::table('articles')->whereIn('id', [1,2,5])->get(); //"select * from `articles` where `id` in (?, ?, ?)"
//
//        $articles = DB::table('articles')->whereNotIn('id', [1,2,5])->get(); //"select * from `articles` where `id` not in (?, ?, ?)"

//        $articles = DB::table('articles')->groupBy('name')->get(); // not work for me
//
//        $articles = DB::table('articles')->take(2)->get(); // "select * from `articles` limit 2"
//
//        $articles = DB::table('articles')->take(3)->skip(2)->get(); //"select * from `articles` limit 3 offset 2"



//        ------------INSERT----------------
//       $res =  DB::table('articles')->insert([
//            ['name' => 'test 1', 'text' => 'hello', 'img' => 'test'],
//            ['name' => 'test 2', 'text' => 'hello world', 'img' => 'test']
//        ]); //"insert into `articles` (`img`, `name`, `text`) values (?, ?, ?), (?, ?, ?)"
//

//        $res =  DB::table('articles')->insertGetId(['name' => 'ttt2', 'text' => 'hello world', 'img' => 'test']); //"insert into `articles` (`name`, `text`, `img`) values (?, ?, ?)"


//        ------------UPDATE----------------
//        $res = DB::table('articles')->where('id', 9)->update(['name' => 'updated name']); //"update `articles` set `name` = ? where `id` = ?"

//        $res = DB::table('articles')->where('id', 9)->delete(); //"delete from `articles` where `id` = ?"

//        dump($res);


//        $result = DB::table('articles')->get();

//        =========model====================

//        $articles = Article::all(); //"select * from `articles`"

//        $articles = Article::where('id', '>', 4)->get(); //"select * from `articles` where `id` > ?"
//
//        $articles = Article::where('id', '>', 4)->limit(2)->get(); //"select * from `articles` where `id` > ? limit 2"

//        $articles = Article::find(1); //"select * from `articles` where `articles`.`id` = ? limit 1"

//        $articles = Article::where('id', 1)->first(); //"select * from `articles` where `id` = ? limit 1"
//
//        $articles = Article::find([1,3,5]); //"select * from `articles` where `articles`.`id` in (?, ?, ?)"
//
//        $articles = Article::findOrFail(100);

//        $article = new Article;
//        $article->name = 'New Article';
//        $article->text = 'New Text';
//        $article->img = 'New img';
//        $article->save();    //insert new data in column
//
//        $article = Article::find(9);
//        $article->name = 'New1 Article';
//        $article->text = 'New1 Text';
//        $article->img = 'New1 img';
//        $article->save();    //update data in column

//
//        Article::create([
//            'name' => 'lorem',
//            'text' => 'some text',
//            'img' => 'iiimmmmggg'
//            ]); // insert new data in column

//        $article = Article::firstOrCreate([
//            'name' => 'lorem11',
//            'text' => 'some text',
//            'img' => 'iiimmmmggg'
//        ]);
//
//        $article = Article::firstOrNew([
//            'name' => 'fonlorem11',
//            'text' => 'some text',
//            'img' => 'iiimmmmggg'
//        ]);
//        $article->save();


//        $article = Article::find(13);
//        $article->delete();

//        $article = Article::destroy(12);
//        $article = Article::destroy([11, 10]);

//        $article = Article::where('id', '<=', 3)->delete();

//        dump($article);

//        $article = Article::find(8);
//        $article->delete();

//        $articles = Article::withTrashed()->get();
//        foreach ($articles as $article) {
//            if($article->trashed()){
//                $article->restore();
//            }
//        }

//        $articles = Article::onlyTrashed()->restore();


//        $articles = Article::find(5);
//        $articles->forceDelete();
//        dump($articles);


//        dump($article);

//        foreach ($articles as $article){
//            echo $article->name;
//        }


//        ONE TO ONE

//        $user = User::find(1);
//
//        $country = Country::find(1);
//
//        $articles = Article::all();
//        dump($user->country);
//        dump($country->user);

//        ONE TO MANY
//        $user = User::find(1);
//        dump($user->articles);

//        $artticles = Article::find(1);
//        dump($artticles->user);

//        MANY TO MANY
//        $usr = User::find(1);
//        dump($usr->roles);

//        $role = Role::find(1);
//        dump($role->users);


//        greedy query 1
//        $articles = Article::with('user')->get(); // use in references

//        greedy query 2
//        $articles = Article::all();
//        $articles->load('user'); // use load when need load reference to all method
//
//        foreach ($articles as $article){
//            dump($article->user);
//        }

////        greedy query 3
//        $users = User::with('articles', 'roles')->get();
//        foreach ($users as $user){
//            dump($user->articles);
//        }

//        greedy query 3
//        $users = User::has('articles')->get();
//        dump($users);

        $user = User::find(1);

//        $article = new Article([
//            'name' => 'New Article',
//            'text' => 'some text some text some text',
//            'img' => 'img'
//
//        ]);

//        $user->articles()->save($article); // variant 1 when we work with objects

//        $user->articles()->saveMany([
//            new Article([
//                'name' => '11New Article',
//                'text' => '11some text some text some text',
//                'img' => '11img'
//
//            ]),
//            new Article([
//                'name' => '22New Article',
//                'text' => '22some text some text some text',
//                'img' => '22img'
//
//            ]),
//            new Article([
//                'name' => '33New Article',
//                'text' => '33some text some text some text',
//                'img' => '33img'
//
//            ]),
//
//        ]);




//        $user->articles()->create(  // variant 2 when we work with arrays
//            [
//                'name' => '1 New Article1',
//                'text' => '1 some text some text some text',
//                'img' => '1 img'
//
//            ]
//        );



//        $role = new Role([
//            'name' => 'guest'
//        ]);
//
//        $user->roles()->save($role);
//
//
//
//
//
//        $articles = Article::all();
//
//        dump($articles);

//
//        =========
//        $user->articles()->where('id', 12)->update([
//            'name' => 'updated name'
//        ]);

//        ==========

//        edit reference one to one
//        $country = Country::find(1);
//        $user = User::find(2);
//        $country->user()->associate($user);
//        $country->save();


//        edit reference many to many
//        $articles = Article::all();
//        $user = User::find(2);
//        foreach ($articles as $article){
//            $article->user()->associate($user);
//            $article->save();
//        }


//        Create new refference in role_user (many to many)
//        $user = User::find(2);
//        $role_id = Role::find(2)->id;
//        $user->roles()->attach($role_id);
//
//        Delete reference in role_user (many to many)
//        $user = User::find(2);
//        $role_id = Role::find(2)->id;
//        $user->roles()->detach($role_id);


//        $article = Article::find(1);
//        echo $article->name;

        $article = Article::find(1);
//        $article->name = 'Some Text';
//        echo $article->name;
//        $array = ['mkey' => 'hello world'];
//        $article->text = $array;
//        $article->save();

        dump($article->toArray());
        dump($article->toJson());












    }
    public function getArticle($id){
        echo $id;
    }
}
