<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
//    protected $request;
//
//    public function __construct(Request $request){
//        $this->request = $request;
//    }

    public function store(Request $request, $id = false){

//      - check if isset field
//        if($request->has('name')){
//            dump($request->input('name'));
//        }

//     - get only fields
//       $only_request = $request->only('name', 'site');

//     - get array withoul this fields
//       $only_request = $request->except('name', 'site');

//       - get email field
//       dump($request->email);


//        -get path
//        dump($request->path());

//        - check uri
//        if($request->is('contact/*')){
//            dump($request);
//        }

//        get url
//        dump($request->url());

//        get url with get
//        dump($request->fullUrl());

//        get method of request
//        dump($request->method());

//        check type of method
//        if($request->isMethod('post')){
//            dump($request->all());
//        }
//        - get root
//        dump($request->root());

//        - get array of get
//        dump($request->query());

//        - get headers
//        dump($request->header());

//        -server
//        dump($request->server());

//        -uri to array
//        dump($request->segments());

//        $request->flash();
//        $request->flashOnly('site', 'name');



//        if($request->isMethod('post')){
//            return redirect()->route('contact')->withInput();
//        }
//        return redirect()->route('contact');
//        return redirect()->route('contact');

        if($request->isMethod('post')){
//            $rules = [
//                'name' => 'required|max:10',
//                'email' => 'required|email',
//                'check' => 'required',
//            ];
//            $this->validate($request, $rules);
//            dump($request->all());

            $messages = [
                'name.required' => 'ПОЛЕ :attribute обязательно к заполнению!!!!!!!!!!!!!!!!!!!!',
                'check.required' => 'Чекбокс реквіред'
            ];

            $validator = Validator::make($request->all(), [
               'name' => 'required',
//                'email' => 'required'
            ], $messages);


            $validator->sometimes('email', 'required', function ($input){
                return strlen($input->name) >= 10;
            });

            $validator->after(function ($validator){
                $validator->errors()->add('name', 'Дополнитльное сообщение');
            });

           if($validator->fails()){
               $messsages = $validator->errors();
//                if($messsages->has('name')){
//                    dump($messsages->all('<p> :message </p>'));
//                }
//               dump($validator->failed());
//               exit;


               return redirect()->route('contact')->withErrors($validator)->withInput();
           }


        }



        $data = array('title' => 'Contacts');
        return view('default.contact', $data);
    }

    public function show(){
        $data = array('title' => 'Contacts');
        return view('default.contact', $data);
    }

}
