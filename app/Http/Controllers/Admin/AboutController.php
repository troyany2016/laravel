<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class AboutController extends Controller
{
    //
    public function show(){

       if(view()->exists('default.about')){
            $data = array('title' => 'About');
            $view =  view('default.about',  $data)->render();
            return $view;
       }


    }
}
