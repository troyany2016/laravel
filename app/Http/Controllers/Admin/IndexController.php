<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function show(){

//        $data = array('title' => 'Hello, World!!!');
//        return view('default.template', $data);

//        $view = view('default.template');
//        $view->with('title', 'Hello world');

        $array = array(
            'title' => 'Laravel Project',
            'data' => [
                'one' => 'list 1',
                'two' => 'list 2',
                'three' => 'List 3',
                'fourth' => 'list 4',
                'five' => 'list 5'
            ],
            'dataI' => ['list 1', 'list 2', 'list 3', 'list 4', 'list 5'],
            'vbar' => true,
            'script' => '<script>alert("hello")</script>'
        );


        if(view()->exists('default.index')){
            return view('default.index', $array);
        }

    }

}
