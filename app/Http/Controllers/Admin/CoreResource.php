<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoreResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Create webform
        echo __METHOD__;
        print '<form action="/pages" method="POST">';
            print ' <input type="text" name="name">';
             print '<br>';
             print '<textarea name="text" id="" cols="30" rows="10"></textarea>';
             print '<br>';
             print '<input type="submit">';
             echo csrf_field();
         print '</form>';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        echo __METHOD__;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get POST data from index mehod
        print_r($_POST);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // show one item http://laravel.dev/pages/10
        // Виборка з БД тут
        echo __METHOD__;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //http://laravel.dev/pages/10/edit
        // page edit element
        print 'edit element';
//        print __METHOD__;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //редвктірованіє
        //метод пут
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Удаленіє
    }

    public function add(){
        print 'custom methos';
    }
}
