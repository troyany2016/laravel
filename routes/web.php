<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'Admin\IndexController@show']);

//Route::get('/article/{id}', ['as' => 'article', function($id){
//    print $id;
//}]);

Route::get('/page/{cat}/{id}', function ($var1, $var2) {
    print $var1;
    print $var2;
});



Route::any('/comments', function (){
    print_r($_GET);
    print_r($_POST);
});

Route::group(['prefix' => 'admin/{id}'], function (){




});

Route::get('/articles', ['uses' => 'Admin\Core@getArticles', 'as' => 'articles']);
Route::get('/article/{id}', [ 'uses' => 'Admin\Core@getArticle', 'as' => 'article']);
Route::get('about', ['uses' => 'Admin\AboutController@show', 'as' => 'about']);

//Route::get('pages/add', 'Admin\CoreResource@add');
//Route::resource('/pages', 'Admin\CoreResource');

//Route::controller('/pages', 'PagesController');

//Route::controller('/pages', 'PagesController');

Route::get('login', [ 'as' => 'login', function(){
    echo '1212';
}]);


//Route::match(['get', 'post'], 'contact/{id?}', ['uses' =>'Admin\ContactController@show', 'as' => 'contact']);
Route::get('/contact' , ['middleware' => ['auth'], 'uses' =>'Admin\ContactController@show', 'as' => 'contact']);
Route::post('/contact', ['uses' =>'Admin\ContactController@store']);

Auth::routes();
//
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth']], function (){
    Route::get('/', ['uses' => 'Admin\AdminController@show', 'as' => 'admin_index']);
    Route::get('add/post', ['uses' => 'Admin\AdminPostController@create', 'as' => 'admin_add_post']);
});
